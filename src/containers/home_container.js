import React, { Component } from 'react';
import {connect} from 'react-redux';
import {getFames} from '../actions';
import FameItem from '../widgets/fame_item';
import { Redirect } from 'react-router-dom';


class HomeContainer extends Component {

  componentWillMount () {
    const jwt = localStorage.getItem('Token')
    console.log('jwt',jwt)
    this.props.dispatch(getFames(jwt,''))
  }
  renderItems = (fames) =>(
    fames.list ?
    fames.list.map( (fame,i) => (
      <FameItem key={i} {...fame} />
    )) :
    null
  )

  loadmore = () => {
    // let count = this.props.books.list.length;
    const jwt = localStorage.getItem('Token')
    console.log('jwt',jwt);
     this.props.dispatch(getFames(jwt))
  }

  render() {

    return (
      <div>
          {this.renderItems(this.props.fames)}
          <div className="loadmore"
             onClick={this.loadmore}
            >
            LoadMore
          </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    fames:state.fames
  }
}

export default connect(mapStateToProps)(HomeContainer);
