import React from 'react';
import axios from 'axios';

const Logout =  (props) => {
  axios.get('https://halloffame-server.herokuapp.com/logout')
                .then(request =>{
                  localStorage.removeItem('Token')
                  setTimeout(()=>{
                    props.history.push('/')
                  },1000)
                })
  localStorage.removeItem('Token')
  setTimeout(()=>{
    props.history.push('/')
  },1000)
  return(
    <div className="logout_container">
          <h1>
               ): Good Luck
          </h1>
    </div>
  )

}


export default Logout;
