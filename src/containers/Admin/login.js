import React ,{Component} from 'react';
import {connect} from 'react-redux';
import {loginUser} from  '../../actions';


class Login extends Component {

  state = {
    password:'',
    username:'',
    error:'',
    success:''
  }
  handleInputEmail = (event) => {
    this.setState({ username:event.target.value })
  }
  handleInputPassword = (event) => {
    this.setState( {password:event.target.value} )
  }

  componentWillReceiveProps (nextProps) {
    if(nextProps.user.login) {
      this.props.history.push('/');
    }
  }

  submitForm = (e)=>{
      e.preventDefault();
      this.props.dispatch(loginUser(this.state))
  }
  render (){
    const {login} = this.props.user.login;
    console.log(this.props,'login')
    return (
      <div className="rl_container">

         <form action="" onSubmit={this.submitForm}>
             <h2>Login Here </h2>
             <div className="form_element">
                  <input type="text"
                         name = "username"
                         value={this.state.email}
                         onChange={this.handleInputEmail}
                         placeholder="Enter your email"/>
             </div>

             <div className="form_element">
                  <input name="password" type="password" value={this.state.password} onChange={this.handleInputPassword}
                   placeholder="Enter your password" />
             </div>
             <button type="submit">Login</button>

             <div className="error">
                  {
                   this.props.user.login===false ?
                         <div>email or password incorrect</div>
                         : null
                  }

             </div>
         </form>

      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    user:state.user
  }
}

export default connect(mapStateToProps)(Login);
