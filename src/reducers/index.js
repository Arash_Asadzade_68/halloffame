import {combineReducers} from 'redux';
import fames from './fames_reducer';
import user from './user_reducer';

const rootReducer = combineReducers({
  fames,
  user
});



export default rootReducer;
