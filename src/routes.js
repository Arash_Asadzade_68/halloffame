import React from 'react';
import {Switch , Route } from 'react-router-dom';
import Layout from './hoc/layout';


import Home from './components/Home/home';
import FameView from './components/Fames';
import Login from './containers/Admin/login';
import Auth from './hoc/auth';
import User from './components/Admin';
import UserRegister from './containers/Admin/register';
import Logout from './containers/Admin/logout';


const Routes = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact component={Auth(Home , true)}/>
        <Route path="/fames/:id" exact component={Auth(FameView,true)} />
        <Route path="/login" exact component = {Auth(Login,false)} />
        <Route path="/logout" exact component = {Auth(Logout,true)} />
        <Route path="/user" exact component = {Auth(User,true)} />
        <Route path="/user/register" exact component ={Auth(UserRegister,true)} />
      </Switch>
    </Layout>
  )
}

export default Routes;
