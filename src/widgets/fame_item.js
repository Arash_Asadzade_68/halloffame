import React from 'react';
import {Link} from 'react-router-dom';


const FameItem = (fame) => (

   <Link to={`/fames/${fame.id}`} className="fame_item">
          <div className="fame_header">
                 <h2>{fame.name}</h2>
          </div>
          <div className="fame_items">
                  <div className="fame_dob">{fame.dob}</div>
                  <div className="fame_image">
                         <img width={100} height={100} src={fame.image}/>
                  </div>
          </div>
   </Link>
)


export default FameItem;
