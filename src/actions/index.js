import axios from 'axios';

export const getFames = async (jwt,list='') => {
  console.log('jwt',jwt)
  const request = await axios.get(
    'https://halloffame-server.herokuapp.com/fames?page=0',
    {
      headers: {
        Authorization: `Bearer ${jwt}`,
        'Content-Type':'application/json',
        Accept:'application/json'
      }
    })
    .then(response => response.data.data.list
    );
  console.log('request', request);
  return {
    type: 'GET_FAMES',
    payload: request
  }
}



export const getFame = async (id,jwt) => {
  const request = await axios.get(`https://halloffame-server.herokuapp.com/fames/${id}`,
  {
   
      headers: {
        Authorization: `Bearer ${jwt}`,
        'Content-Type':'application/json',
        Accept:'application/json'
      }
  })
    .then(response => response.data.data)
  return {
    type: 'GET_FAME',
    payload: request
  }
}




//*============== USER ===============*//

export const loginUser = async ({ username, password }) => {
  const request = await axios.post(`https://halloffame-server.herokuapp.com/login`, { username, password })
    .then(response => response)

  if (request.data.data.success)
    localStorage.setItem('Token', request.headers.authorization)
  return {
    type: 'USER_LOGIN',
    payload: request.data.data.success
  }
}


export const auth = () => {
  const jwt = localStorage.getItem('Token')
  if (jwt)
    return {
      type: 'USER_AUTH',
      payload: {
        isAuth: true
      }
    }
  else return {
    type: 'USER_AUTH',
    payload: {
      isAuth: false
    }
  }
}

export const userRegister = (user, userlist) => {
  const request = axios.post('/api/register', user)

  return (dispatch) => {

    request.then(({ data }) => {
      let users = data.success ? [...userlist, data.user] : userlist;
      let response = {
        success: data.success,
        users
      }

      dispatch({
        type: 'USER_REGISTER',
        payload: response
      })
    })
  }
}
