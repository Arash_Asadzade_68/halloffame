import React , {Component} from 'react';
import {connect} from 'react-redux';
import {getFame} from '../../actions';

class FameView extends Component {
  state = {
    fame :{}
  }

  componentWillMount () {
    const jwt = localStorage.getItem('Token')
    this.props.dispatch (getFame(this.props.match.params.id,jwt))
  }

  componentWillReceiveProps (nextProps) {
     if(nextProps.fames.fame) this.setState({fame:nextProps.fames.fame})
  }

  renderfame = (fame) => (

    fame ?
      <div className="br_container">
          <div className="br_header">
                <h2>{fame.name}</h2>
          </div>
          <div className="dob">
                {fame.dob}
          </div>
          <div className="br_box">
                <div className="left">
                   <div>
                       <span>photo:</span> <img src={fame.image}/>
                   </div>
                </div>
          </div>
      </div>
      :null

  )
  render() {
    return (
      <div>
         {this.renderfame(this.state.fame)}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
   return {
     fames:state.fames
   }
}

export default connect(mapStateToProps) (FameView);
